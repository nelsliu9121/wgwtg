import { PostCard } from '@components/posts/PostCard';
import { Post } from '@core/typings/Post';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import * as React from 'react';

const post: Post = {
  id: '0',
  name: 'Test',
  content:
    '<p>許秀雯解釋，「專法公投」包含兩個事項，其一是前提為「婚姻定義是一男一女的結合」，其二是「要求另立專法來規範同性別二人的結合關係」，明顯牴觸公投法一案一事項原則。</p>',
  createdAt: Date.now(),
  location: null,
  category: {
    id: '1',
    name: '測試公告',
  },
};

storiesOf('Posts', module).add('PostCard', () => {
  return (
    <PostCard post={post} setPostPinned={action('PostCard setPostPinned')} />
  );
});
