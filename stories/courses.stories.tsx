import { CourseCard } from '@components/courses/CourseCard';
import { Course } from '@core/typings/Course';
import { action } from '@storybook/addon-actions';
import { storiesOf } from '@storybook/react';
import List from 'material-ui/List';
import * as React from 'react';

// import { linkTo } from '@storybook/addon-links';

const mockCourse: Course = {
  id: '12635',
  subjectId: '80',
  name: '終極踏板(Body Step)',
  alias: 'Body Step',
  teacher: 'Torrance',
  startTime: '07:10',
  endTime: '08:10',
  category: {
    id: '7',
    name: '萊美系統課程(LesMills)',
    color: '#ed0145',
  },
};

storiesOf('Courses', module).add('Card', () => (
  <List>
    <CourseCard
      course={mockCourse}
      inStarList={false}
      addToStarList={action('Courses addToStarList')}
      removeFromStarList={action('Courses removeFromStarList')}
    />
  </List>
));
