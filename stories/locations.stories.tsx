import { LocationCard } from '@components/locations/LocationCard';
import { LocationInfo } from '@components/locations/LocationInfo';
import { hydrateLocations } from '@core/hydrators/hydrateLocations';
import { Location } from '@core/typings/Location';
import { storiesOf } from '@storybook/react';
import * as React from 'react';

import { mockLocation } from './mocks/location';

// import { action } from '@storybook/addon-actions';
// import { linkTo } from '@storybook/addon-links';

const location: Location = hydrateLocations([mockLocation]).first();

storiesOf('Locations', module).add('Card', () => (
  <LocationCard location={location} inStarList={true} />
));

storiesOf('Locations', module).add('Info', () => (
  <LocationInfo location={location} />
));
