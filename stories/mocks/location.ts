export const mockLocation = {
  'address' : '民權東路6段180巷6號B2',
  'alias' : 'Neihu Fuhwa',
  'city' : '台北市',
  'geo' : {
    'lat' : 25.0681796,
    'long' : 121.5924716
  },
  'group' : '1',
  'id' : '37',
  'map' : 'goo.gl/BqKreY',
  'name' : '內湖福華店',
  'phone' : '02-2790-5522',
  'photo' : 'clubs/FH/1客服櫃檯.jpg',
  'rank' : '9',
  'rooms' : {
    '1' : {
      'id' : '1',
      'name' : 'CYCLING STUDIO'
    },
    '4' : {
      'id' : '4',
      'name' : 'GROUP STUDIO A'
    }
  },
  'status' : 0,
  'zipcode' : '114 台北市 內湖區'
};
