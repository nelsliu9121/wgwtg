import 'module-alias/register';

import * as express from 'express';
import * as Next from 'next';
import { join } from 'path';

import { pagesRouterMaker } from './server/pages';
import { apiRoutes } from './server/routes';

const port = parseInt(process.env.PORT, 10) || 3000;
const dev = process.env.NODE_ENV !== 'production';
const dir = dev ? 'src' : 'dist';
const app = Next({ dev, dir });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();

  server.use(express.json());

  server.get('/service-worker.js', (req, res) => {
    const filePath = join(__dirname, '.next', req.url);
    app.serveStatic(req, res, filePath);
  });

  server.use('/api', apiRoutes);

  server.use(pagesRouterMaker(app));

  server.get('*', (req, res) => {
    return handle(req, res);
  });

  server.listen(port, (err) => {
    if (err) {
      throw err;
    }
    /* tslint:disable-next-line:no-console */
    console.log(`> Ready on http://localhost:${port}`);
  });
});
