import { TypedAction } from 'redoodle';

import { CoursesType } from '../constants/Courses';
import { Schedule } from '../typings/Schedule';

export const getCourses = TypedAction.define(CoursesType.GET_COURSES)<{
  location: string;
  room: string;
  month: number;
}>();

export const getCoursesSuccess = TypedAction.define(
  CoursesType.GET_COURSES_SUCCESS
)<{
  schedule: Schedule;
}>();

export const getCoursesFailed = TypedAction.define(
  CoursesType.GET_COURSES_FAILED
)<{ error: Error }>();
