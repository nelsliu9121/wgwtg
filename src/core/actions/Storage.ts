import { TypedAction } from 'redoodle';

import { StorageType } from '../constants/Storage';

export const loadSuccess = TypedAction.defineWithoutPayload(
  StorageType.LOAD_SUCCESS
)();
