import * as CoursesCreators from '@core/actions/Courses';
import * as LocationsCreators from '@core/actions/Locations';

export const actions = {
  ...LocationsCreators,
  ...CoursesCreators,
};
