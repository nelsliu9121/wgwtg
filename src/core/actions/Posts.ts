import { News, NewsCategory } from '@core/typings/News';
import { List } from 'immutable';
import { TypedAction } from 'redoodle';

import { PostsType } from '../constants/Posts';
import { Post } from '../typings/Post';

export const getPosts = TypedAction.define(PostsType.GET_POSTS)<{
  location: string;
}>();

export const getPostsSuccess = TypedAction.define(PostsType.GET_POSTS_SUCCESS)<{
  posts: List<Post>;
}>();

export const getPostsFailed = TypedAction.define(PostsType.GET_POSTS_FAILED)<{
  error: Error;
}>();

export const getNews = TypedAction.defineWithoutPayload(PostsType.GET_NEWS)();

export const getNewsSuccess = TypedAction.define(PostsType.GET_NEWS_SUCCESS)<{
  categories: List<NewsCategory>;
  news: List<News>;
}>();

export const getNewsFailed = TypedAction.define(PostsType.GET_NEWS_FAILED)<{
  error: Error;
}>();
