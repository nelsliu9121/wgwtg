import { List } from 'immutable';
import { TypedAction } from 'redoodle';

import { LocationsType } from '../constants/Locations';
import { Location } from '../typings/Location';

export const getLocations = TypedAction.defineWithoutPayload(LocationsType.GET_LOCATIONS)();

export const getLocationsSuccess = TypedAction.define(LocationsType.GET_LOCATIONS_SUCCESS)<{
  locations: List<Location>;
}>();

export const getLocationsFailed = TypedAction.define(LocationsType.GET_LOCATIONS_FAILED)<{
  error: Error;
}>();
