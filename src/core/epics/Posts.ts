import { getNewsFailed, getNewsSuccess, getPostsFailed, getPostsSuccess } from '@core/actions/Posts';
import { PostsType } from '@core/constants/Posts';
import { hydratePosts } from '@core/hydrators/hydratePosts';
import { BasicActionPayload } from '@core/typings/actions/BasicActionPayload';
import { EpicDependency } from '@core/typings/epics/EpicDependency';
import { News, NewsCategory } from '@core/typings/News';
import { Post } from '@core/typings/Post';
import { RootState } from '@core/typings/reducers/RootState';
import { List } from 'immutable';
import { Epic } from 'redux-observable';
import { of as ObservableOf } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

export const getPostsByLocationEpic: Epic<BasicActionPayload, any, RootState, EpicDependency> = (
  action$,
  store$,
  { apiExecutor }
) => {
  return action$.ofType(PostsType.GET_POSTS).pipe(
    switchMap((action) => {
      const apiObservable = apiExecutor.call<[Post]>({
        method: 'GET',
        url: `/posts/by-location/${action.payload.location}`,
      });

      return apiObservable.pipe(
        map<[Post], BasicActionPayload>((response) => {
          return getPostsSuccess({
            posts: hydratePosts(response),
          });
        }),
        catchError((error) => ObservableOf(getPostsFailed({ error })))
      );
    })
  );
};

export const getNewsEpic: Epic<BasicActionPayload, any, RootState, EpicDependency> = (
  action$,
  store$,
  { apiExecutor }
) => {
  return action$.ofType(PostsType.GET_NEWS).pipe(
    switchMap((action) => {
      const apiObservable = apiExecutor.call<{ categories: NewsCategory[]; news: News[] }>({
        method: 'GET',
        url: `/posts/news`,
      });

      return apiObservable.pipe(
        map(({ categories, news }) => {
          return getNewsSuccess({
            news: List(news),
            categories: List(categories),
          });
        }),
        catchError((error) => ObservableOf(getNewsFailed({ error })))
      );
    })
  );
};
