import { RootState } from '@core/typings/reducers/RootState';
import { Epic } from 'redux-observable';
import { of as ObservableOf } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { getCoursesFailed, getCoursesSuccess } from '../actions/Courses';
import { CoursesType } from '../constants';
import { hydrateSchedule } from '../hydrators/hydrateSchedule';
import { BasicActionPayload } from '../typings/actions/BasicActionPayload';
import { TypedActionPayload } from '../typings/actions/TypedActionPayload';
import { DehydratedSchedule } from '../typings/dehydrates/DehydrateSchedule';
import { EpicDependency } from '../typings/epics/EpicDependency';

interface CoursesPayload {
  location: string;
  room: string;
  month: number;
}

export const getCoursesByLocationEpic: Epic<TypedActionPayload<CoursesPayload>, any, RootState, EpicDependency> = (
  action$,
  store$,
  { apiExecutor }
) => {
  return action$.ofType(CoursesType.GET_COURSES).pipe(
    switchMap((action) => {
      const { location, room, month } = action.payload;
      const apiObserable = apiExecutor.call<DehydratedSchedule>({
        method: 'GET',
        url: `/courses/by-office/${location}/${room}/${month}`,
      });

      return apiObserable.pipe(
        map<DehydratedSchedule, BasicActionPayload>((response) => {
          return getCoursesSuccess({
            schedule: hydrateSchedule(response),
          });
        })
      );
    }),
    catchError((error) => ObservableOf(getCoursesFailed({ error })))
  );
};
