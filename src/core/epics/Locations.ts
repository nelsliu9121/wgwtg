import { Classroom } from '@core/typings/Classroom';
import { RootState } from '@core/typings/reducers/RootState';
import { List, Map } from 'immutable';
import { Epic } from 'redux-observable';
import { of as ObservableOf } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { getLocationsFailed, getLocationsSuccess } from '../actions/Locations';
import { LocationsType } from '../constants';
import { BasicActionPayload } from '../typings/actions/BasicActionPayload';
import { EpicDependency } from '../typings/epics/EpicDependency';
import { Location } from '../typings/Location';

export const getLocationsEpic: Epic<BasicActionPayload, any, RootState, EpicDependency> = (
  action$,
  store$,
  { apiExecutor }
) => {
  return action$.ofType(LocationsType.GET_LOCATIONS).pipe(
    switchMap(() => {
      const apiObservable = apiExecutor.call<[Location]>({
        method: 'GET',
        url: '/locations',
      });

      return apiObservable.pipe(
        map<[Location], BasicActionPayload>((response) => {
          const result: List<Location> = List(response)
            .map((location) => {
              location.rooms = Map<string, Classroom>(location.rooms).toList();
              return location;
            })
            .toList();
          return getLocationsSuccess({
            locations: result,
          });
        })
      );
    }),
    catchError((error) => ObservableOf(getLocationsFailed({ error })))
  );
};
