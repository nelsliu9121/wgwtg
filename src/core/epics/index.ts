import * as CourseEpics from '@core/epics/Courses';
import * as LocationEpics from '@core/epics/Locations';
import * as PostEpics from '@core/epics/Posts';
import { combineEpics } from 'redux-observable';

export const epics = [
  ...Object.keys(LocationEpics).map((k) => LocationEpics[k]),
  ...Object.keys(CourseEpics).map((k) => CourseEpics[k]),
  ...Object.keys(PostEpics).map((k) => PostEpics[k]),
];
export const combinedEpics = combineEpics(...epics);
