export function stripStyle(html: string): string {
  const regex = new RegExp(/\sstyle\=\"[a-zA-Z0-9\:\;\-\#]*\"/gi);
  return html.replace(regex, '');
}
