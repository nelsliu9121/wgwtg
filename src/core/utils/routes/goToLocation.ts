import { Location } from '@core/typings/Location';
import { goToPage } from '@core/utils/routes/goToPage';

export function goToLocation(location: Location): Promise<boolean> {
  return goToPage(
    {
      pathname: '/locations/view',
      query: { location: location.id },
    },
    `/locations/${location.id}`
  );
}
