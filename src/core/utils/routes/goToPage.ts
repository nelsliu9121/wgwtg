import Router from 'next/router';
import { UrlObject } from 'url';

export function goToPage(url: string | UrlObject, as?: string) {
  const promise = Router.push(url, as);
  promise.catch(console.warn);
  return promise;
}
