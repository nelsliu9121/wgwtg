import { Classroom } from '@core/typings/Classroom';
import { Location } from '@core/typings/Location';
import { goToPage } from '@core/utils/routes/goToPage';

export function goToCourseByClassroom(location: Location, room: Classroom): Promise<boolean> {
  return goToPage(
    {
      pathname: '/courses/by-location',
      query: {
        location: location.id,
        room: room.id,
      },
    },
    `/courses/by-location/${location.id}/${room.id}`
  );
}
