import { Response } from 'express';
import { Observable } from 'rxjs';

export function ErrorHandler(res?: Response): (err: Error) => void {
  if (res) {
    return (err) => {
      console.error(err);
      res.writeHead(500, err.message);
      res.end();
    };
  }
  return console.error;
}

export function ErrorHandlerWithObservable(
  res?: Response
): (err: Error) => Observable<any> {
  if (res) {
    return (err) => {
      console.error(err);
      res.writeHead(500, err.message);
      res.end();
      return new Observable();
    };
  }
  return (err) => {
    console.error(err);
    return new Observable();
  };
}
