import { List, Map } from 'immutable';

export function listToMap<T, K = number>(
  list: List<T>,
  keyMaker?: (K: T) => K
): Map<K | number, T> {
  return list.reduce<Map<K | number, T>>((prev, curr, key?) => {
    return prev.set(keyMaker ? keyMaker(curr) : key, curr);
  }, Map());
}
