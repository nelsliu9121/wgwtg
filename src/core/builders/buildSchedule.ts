import { buildLocation } from '@core/builders/buildLocations';
import { WeekdayName, WeekdayNameType } from '@core/typings/common/WeekdayName';
import { Course } from '@core/typings/Course';
import { CourseAPIResponse, CourseResponse } from '@core/typings/responses/CourseResponse';
import { Schedule } from '@core/typings/Schedule';
import { List, Map } from 'immutable';

function buildCategory(
  category: CourseResponse['subject']['course']
): CourseResponse['subject']['course'] {
  return {
    id: category.id,
    name: category.name,
    color: category.color,
  };
}

export function buildCourses(courses: [CourseResponse]): List<Course> {
  return List(courses)
    .map<Course>((course) => ({
      id: course.id,
      subjectId: course.subject.id,
      name: course.subject.name,
      alias: course.subject.alias,
      teacher: course.remark,
      startTime: course.start_at,
      endTime: course.end_at,
      category: buildCategory(course.subject.course),
    }))
    .toList();
}

export function buildSchedule(
  periods: CourseAPIResponse['periods']
): Map<WeekdayNameType, List<Course>> {
  return Map(periods)
    .mapEntries<WeekdayNameType, List<Course>>((entry) => {
      const weekdayName = WeekdayName.get(entry[0]);
      return [weekdayName, buildCourses(entry[1])];
    })
    .toMap();
}

export function buildSchedules(
  response: CourseAPIResponse,
  month: string,
  year: string
): Schedule {
  return {
    location: buildLocation(response.office),
    room: {
      id: response.id,
      name: response.name,
    },
    month,
    year,
    schedule: buildSchedule(response.periods),
  };
}
