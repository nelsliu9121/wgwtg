import {
  LocationAPIResponse,
  LocationResponse,
} from '../typings/responses/LocationResponse';
import { List } from 'immutable';
import { Location, LocationStatus } from '../typings/Location';
import { Classroom } from '../typings/Classroom';

export function buildRooms(
  classrooms: [{ id: string; name: string }]
): List<Classroom> {
  return List(classrooms)
    .map((room) => ({
      id: room.id,
      name: room.name,
    }))
    .toList();
}

export function buildLocation(location: LocationResponse): Location {
  return {
    id: location.id,
    rank: location.rank,
    status:
      location.status === 'visible'
        ? LocationStatus.VISIBLE
        : LocationStatus.HIDDEN,
    name: location.name,
    alias: location.alias,
    phone: location.phone,
    zipcode: location.zipcode,
    address: location.address,
    map: location.map,
    group: location.office_city_id,
    photo: location.image_path,
    city: location.zip_city ? location.zip_city : null,
    rooms: location.classrooms ? buildRooms(location.classrooms) : null,
    geo: {
      long: location.geocoding[0].geometry.location.lng,
      lat: location.geocoding[0].geometry.location.lat,
    },
  };
}

export function buildLocations(response: LocationAPIResponse): List<Location> {
  return List(response)
    .map(buildLocation)
    .toList();
}
