import { News } from '@core/typings/News';
import { NewsArticleResponse } from '@core/typings/responses/NewsResponse';
import { getTime } from 'date-fns';
import { List } from 'immutable';

export function buildNews(response: NewsArticleResponse[][]): List<News> {
  return List(response)
    .reduce<List<NewsArticleResponse>>((accu, curr) => accu.concat(curr).toList(), List())
    .map<News>((n) => ({
      id: n.id,
      title: n.title,
      content: n.details,
      image: n.image_path,
      time: getTime(n.released_at),
      categoryId: parseInt(n.category.id, 10),
    }))
    .toList();
}
