import { NewsCategory } from '@core/typings/News';
import { List } from 'immutable';

export function buildNewsCategories(response: NewsCategory[]): List<NewsCategory> {
  return List(response)
    .map<NewsCategory>((category) => ({
      id: category.id,
      name: category.name,
      alias: category.alias,
    }))
    .toList();
}
