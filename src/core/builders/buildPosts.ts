import { buildLocation } from '@core/builders/buildLocations';
import { Post } from '@core/typings/Post';
import { PostResponse, PostsResponse } from '@core/typings/responses/PostResponse';
import { getTime } from 'date-fns';
import { List } from 'immutable';

export function buildPost(post: PostResponse): Post {
  return {
    id: post.id,
    name: post.title,
    content: post.details,
    createdAt: getTime(post.created_at),
    location: buildLocation(post.office),
    category: {
      id: post.category.id,
      name: post.category.name,
    },
  };
}

export function buildPosts(response: PostsResponse): List<Post> {
  return List(response.data)
    .map(buildPost)
    .toList();
}
