import { setWith, TypedReducer } from 'redoodle';

import { getCoursesSuccess } from '../actions/Courses';
import { Schedule } from '../typings/Schedule';

export interface CourseState {
  schedule: Schedule;
}

export const initialCourseState: CourseState = {
  schedule: null,
};

export const CourseReducerHandlers = TypedReducer.builder<CourseState>()
  .withHandler(getCoursesSuccess.TYPE, (state, { schedule }) => {
    return setWith(state, {
      schedule,
    });
  })
  .withDefaultHandler((state) => state);

export const CourseReducer = CourseReducerHandlers.build();
