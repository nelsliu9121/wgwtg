import { List } from 'immutable';
import { setWith, TypedReducer } from 'redoodle';

import { getLocationsSuccess } from '../actions/Locations';
import { Location } from '../typings/Location';

export interface LocationState {
  locations: List<Location>;
}

export const initialLocationState: LocationState = {
  locations: List<Location>(),
};

export const LocationReducerHandlers = TypedReducer.builder<LocationState>()
  .withHandler(getLocationsSuccess.TYPE, (state, { locations }) => {
    return setWith(state, {
      locations,
    });
  })
  .withDefaultHandler((state) => state);

export const LocationReducer = LocationReducerHandlers.build();
