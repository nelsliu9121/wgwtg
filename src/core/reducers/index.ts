import { CourseReducer, initialCourseState } from '@core/reducers/Courses';
import { initialLocationState, LocationReducer } from '@core/reducers/Locations';
import { initialPostState, PostReducer } from '@core/reducers/Posts';
import { RootState } from '@core/typings/reducers/RootState';
import { combineReducers, ReducerMap } from 'redoodle';

export const reducers: ReducerMap<RootState> = {
  Locations: LocationReducer,
  Courses: CourseReducer,
  Posts: PostReducer,
};

export function initializeState(): RootState {
  return {
    Locations: initialLocationState,
    Courses: initialCourseState,
    Posts: initialPostState,
  };
}

export const combinedReducers = combineReducers<RootState>(reducers);
