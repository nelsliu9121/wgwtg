import { News, NewsCategory } from '@core/typings/News';
import { List } from 'immutable';
import { setWith, TypedReducer } from 'redoodle';

import { getNewsSuccess, getPostsSuccess } from '../actions/Posts';
import { Post } from '../typings/Post';

export interface PostState {
  posts: List<Post>;
  news: List<News>;
  newsCategories: List<NewsCategory>;
}

export const initialPostState: PostState = {
  posts: List<Post>(),
  news: List<News>(),
  newsCategories: List<NewsCategory>(),
};

export const PostReducerHandlers = TypedReducer.builder<PostState>()
  .withHandler(getPostsSuccess.TYPE, (state, { posts }) => {
    return setWith(state, {
      posts,
    });
  })
  .withHandler(getNewsSuccess.TYPE, (state, { news, categories }) => {
    return setWith(state, {
      news,
      newsCategories: categories,
    });
  })
  .withDefaultHandler((state) => state);

export const PostReducer = PostReducerHandlers.build();
