import { colorStyles } from '@core/styles/colorStyles';
import { createGenerateClassName, createMuiTheme } from '@material-ui/core/styles';
import { SheetsRegistry } from 'jss';

const globalAny: any = global;
const processAny: any = process;

// A theme with custom primary and secondary color.
// It's optional.
const theme = createMuiTheme({
  typography: {
    fontFamily: `'Noto Sans', sans-serif`,
  },
  palette: {
    primary: {
      main: colorStyles.primary,
    },
    secondary: {
      main: colorStyles.secondary,
    },
  },
});

function createPageContext() {
  return {
    theme,
    // This is needed in order to deduplicate the injection of CSS in the page.
    sheetsManager: new Map(),
    // This is needed in order to inject the critical CSS.
    sheetsRegistry: new SheetsRegistry(),
    // The standard class name generator.
    generateClassName: createGenerateClassName(),
  };
}

export default function getPageContext() {
  // Make sure to create a new context for every server-side request so that data
  // isn't shared between connections (which would be bad).
  if (!processAny.browser) {
    return createPageContext();
  }

  // Reuse context on the client-side.
  if (!globalAny.__INIT_MATERIAL_UI__) {
    globalAny.__INIT_MATERIAL_UI__ = createPageContext();
  }

  return globalAny.__INIT_MATERIAL_UI__;
}
