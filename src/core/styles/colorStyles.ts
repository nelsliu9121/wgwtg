export const colorStyles = {
  primary: '#004e98',
  primaryAccented: '#3a6ea5',
  secondary: '#c0c0c0',
  secondaryAccented: '#ebebeb',
  accented: '#ff6700',
};
