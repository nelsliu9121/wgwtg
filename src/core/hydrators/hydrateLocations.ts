import { Classroom } from '@core/typings/Classroom';
import { List, Map } from 'immutable';

export function hydrateLocations(locations: [any]): any {
  return List(locations)
    .map((location) => {
      location.rooms = Map<Classroom>(location.rooms).toList();
      return location;
    })
    .toList();
}
