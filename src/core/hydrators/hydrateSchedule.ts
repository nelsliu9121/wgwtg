import { List, Map } from 'immutable';

import { WeekdayNameType } from '../typings/common/WeekdayName';
import { Course } from '../typings/Course';
import { DehydratedSchedule } from '../typings/dehydrates/DehydrateSchedule';
import { Schedule } from '../typings/Schedule';

export function hydrateSchedule(schedule: DehydratedSchedule): Schedule {
  return {
    location: schedule.location,
    room: schedule.room,
    month: schedule.month,
    year: schedule.year,
    schedule: Map<WeekdayNameType, [Course]>(schedule.schedule)
      .map((course) => List(course))
      .toMap(),
  };
}
