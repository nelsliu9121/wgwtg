export interface BasicActionPayload {
  type: string;
  payload?: any;
  error?: boolean;
  meta?: any;
}
