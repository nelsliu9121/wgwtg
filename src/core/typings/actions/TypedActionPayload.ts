export interface TypedActionPayload<T = any> {
  type: string;
  payload?: { error?: Error } & Partial<T>;
  error?: boolean;
  meta?: any;
}
