import { Location } from '../Location';

export interface LocationsSnapshot {
  locations: { [S: string]: Location };
  lastUpdated: number;
}
