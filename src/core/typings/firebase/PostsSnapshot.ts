import { Post } from '@core/typings/Post';

export interface PostsSnapshot {
  posts: Post[];
  lastUpdated: number;
}
