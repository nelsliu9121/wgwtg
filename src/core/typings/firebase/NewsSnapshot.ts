import { News } from '@core/typings/News';

export interface NewsSnapshot {
  categories: string[];
  news: News[];
  lastUpdated: number;
}
