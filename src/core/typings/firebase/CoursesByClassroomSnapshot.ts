import { Schedule } from '../Schedule';

export interface CoursesByClassroomSnapshot {
  [classrom: string]: Schedule;
}
