import { Classroom } from '@core/typings/Classroom';
import { List } from 'immutable';

export enum LocationStatus {
  VISIBLE,
  HIDDEN,
}

export type Location = {
  id: string;
  rank: string;
  status: LocationStatus;
  name: string;
  alias: string;
  phone: string;
  zipcode: string;
  address: string;
  map: string;
  photo: string;
  city: string;
  group: string;
  rooms: List<Classroom>;
  geo: {
    long: number;
    lat: number;
  };
};
