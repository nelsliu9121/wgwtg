import { Classroom } from '../Classroom';
import { Course } from '../Course';
import { Location } from '../Location';

export interface DehydratedSchedule {
  location: Location;
  room: Classroom;
  month: string;
  year: string;
  schedule: { [K: string]: [Course] };
}
