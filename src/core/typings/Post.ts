import { Location } from '@core/typings/Location';

export interface Post {
  id: string;
  name: string;
  content: string;
  createdAt: number;
  location: Location;
  category: PostCategory;
}

interface PostCategory {
  id: string;
  name: string;
}
