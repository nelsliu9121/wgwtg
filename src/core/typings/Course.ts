export interface Course {
  id: string;
  subjectId: string;
  name: string;
  alias: string;
  teacher: string;
  startTime: string;
  endTime: string;
  category: Category;
}

export interface Category {
  id: string;
  name: string;
  color: string;
}
