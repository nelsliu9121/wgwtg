import { FirebaseApiExecutor } from '../../api/FirebaseApiExecutor';
import { ImageDownloader } from '../../api/ImageDownloader';
import { ServerApiExecutor } from '../../api/ServerApiExecutor';

export interface RouteDependencies {
  apiExecutor: ServerApiExecutor;
  firebaseExecutor: FirebaseApiExecutor;
  imageDownloader?: ImageDownloader;
}
