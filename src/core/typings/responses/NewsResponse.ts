import { NewsCategory } from '@core/typings/News';

export interface NewsCategoriesResponse {
  data: Array<NewsCategory>;
}

export interface NewsResponse {
  data: NewsArticleResponse[];
}

export interface NewsArticleResponse {
  id: string;
  title: string;
  details: string;
  image_path: string;
  released_at: string;
  category: NewsCategory;
}
