import { LocationResponse } from '@core/typings/responses/LocationResponse';

export interface PostsResponse {
  data: PostResponse[];
}

export interface PostResponse {
  id: string;
  title: string;
  details: string;
  created_at: string;
  released_at: string;
  category: {
    id: string;
    name: string;
  };
  office: LocationResponse;
}
