export type LocationAPIResponse = [LocationResponse];

export interface LocationResponse {
  id: string;
  rank: string;
  status: string;
  name: string;
  alias: string;
  phone: string;
  zipcode: string;
  address: string;
  map: string;
  office_city_id: string;
  image_path: string;
  zip_city: string;
  classrooms: [
    {
      id: string;
      name: string;
    }
  ];
  geocoding: [
    {
      geometry: {
        location: {
          lng: number;
          lat: number;
        };
      };
    }
  ];
}
