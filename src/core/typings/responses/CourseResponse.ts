import { LocationResponse } from '@core/typings/responses/LocationResponse';

export interface CourseAPIResponse {
  office: LocationResponse;
  name: string;
  id: string;
  periods: { [S: string]: [CourseResponse] };
}

export interface CourseResponse {
  id: string;
  subject: {
    id: string;
    name: string;
    alias: string;
    course: {
      id: string;
      name: string;
      color: string;
    };
  };
  remark: string;
  day_week: string;
  start_at: string;
  end_at: string;
}
