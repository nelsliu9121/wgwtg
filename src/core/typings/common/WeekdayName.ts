import { List } from 'immutable';

export type WeekdayNameType = string;

export class WeekdayName {
  public static Monday: WeekdayNameType = 'Monday';
  public static Tuesday: WeekdayNameType = 'Tuesday';
  public static Wednesday: WeekdayNameType = 'Wednesday';
  public static Thursday: WeekdayNameType = 'Thursday';
  public static Friday: WeekdayNameType = 'Friday';
  public static Saturday: WeekdayNameType = 'Saturday';
  public static Sunday: WeekdayNameType = 'Sunday';

  public static get All(): List<string> {
    return List([
      WeekdayName.Monday,
      WeekdayName.Tuesday,
      WeekdayName.Wednesday,
      WeekdayName.Thursday,
      WeekdayName.Friday,
      WeekdayName.Saturday,
      WeekdayName.Sunday,
    ]);
  }

  public static get(input: string | number): WeekdayNameType {
    switch (input) {
      case 'Monday':
      case 1:
      case '1':
        return WeekdayName.Monday;
      case 'Tuesday':
      case 2:
      case '2':
        return WeekdayName.Tuesday;
      case 'Wednesday':
      case 3:
      case '3':
        return WeekdayName.Wednesday;
      case 'Thursday':
      case 4:
      case '4':
        return WeekdayName.Thursday;
      case 'Friday':
      case 5:
      case '5':
        return WeekdayName.Friday;
      case 'Saturday':
      case 6:
      case '6':
        return WeekdayName.Saturday;
      case 'Sunday':
      case 7:
      case '7':
        return WeekdayName.Sunday;
      default:
        return WeekdayName.Monday;
    }
  }
}
