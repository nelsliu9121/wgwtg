import { Classroom } from '@core/typings/Classroom';
import { WeekdayNameType } from '@core/typings/common/WeekdayName';
import { Course } from '@core/typings/Course';
import { Location } from '@core/typings/Location';
import { List, Map } from 'immutable';

export interface Schedule {
  location: Location;
  room: Classroom;
  month: string;
  year: string;
  schedule: Map<WeekdayNameType, List<Course>>;
}
