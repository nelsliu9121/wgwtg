import { ClientApiExecutor } from '../../api/ClientApiExecutor';

export interface EpicDependency {
  apiExecutor: ClientApiExecutor;
}
