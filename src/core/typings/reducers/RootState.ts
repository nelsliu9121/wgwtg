import { CourseState } from '@core/reducers/Courses';
import { LocationState } from '@core/reducers/Locations';
import { PostState } from '@core/reducers/Posts';

export interface RootState {
  Locations: LocationState;
  Courses: CourseState;
  Posts: PostState;
}
