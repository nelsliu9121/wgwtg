export interface News {
  id: string;
  title: string;
  content: string;
  image: string;
  time: number;
  categoryId: number;
}

export interface NewsCategory {
  id: string;
  name: string;
  alias: string;
}
