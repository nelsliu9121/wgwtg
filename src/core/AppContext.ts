import { initStore } from '@core/store';
import { RootState } from '@core/typings/reducers/RootState';
import { Store } from 'redoodle';

export class AppContext {
  private static store: Store<RootState>;

  public static getStore() {
    if (!AppContext.store) {
      AppContext.store = initStore();
    }
    return AppContext.store;
  }
}
