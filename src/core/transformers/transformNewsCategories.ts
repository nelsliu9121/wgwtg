import { NewsCategory } from '@core/typings/News';
import { List } from 'immutable';

export function transformNewsCategories(categories: List<NewsCategory>): { [key: string]: NewsCategory } {
  return List(categories).toObject();
}
