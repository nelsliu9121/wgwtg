import { News } from '@core/typings/News';
import { List } from 'immutable';

export function transformNews(news: List<News>): { [key: string]: News } {
  return news.toObject();
}
