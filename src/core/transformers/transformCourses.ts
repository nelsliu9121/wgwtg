import { Schedule } from '../typings/Schedule';
import { listToMap } from '../utils/listToMap';

function transformSchedule(schedule: Schedule['schedule']) {
  return schedule.map((courses) => listToMap(courses).toJS()).toJS();
}

export function transformSchedules(schedules: Schedule) {
  return {
    location: schedules.location,
    room: schedules.room,
    month: schedules.month,
    year: schedules.year,
    schedule: transformSchedule(schedules.schedule),
  };
}
