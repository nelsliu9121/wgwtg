import { List } from 'immutable';

import { Location } from '../typings/Location';
import { listToMap } from '../utils/listToMap';

function transformLocation(location: Location): any {
  const transformed: any = location;
  transformed.rooms = listToMap(location.rooms, (room) => Number(room.id)).toJS();
  // transformed.rooms = location.rooms.toOrderedMap().toJS();
  return transformed;
}

export function transformLocations(locations: List<Location>): { [S: number]: Location } {
  return listToMap(locations, (location) => Number(location.id))
    .map((location) => transformLocation(location))
    .toJS();
}
