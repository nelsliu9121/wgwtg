import { List } from 'immutable';

import { Post } from '../typings/Post';
import { listToMap } from '../utils/listToMap';

export function transformPosts(posts: List<Post>): { [S: string]: Post } {
  return listToMap(posts, (post) => post.id).toJS();
}
