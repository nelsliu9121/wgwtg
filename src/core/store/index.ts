import { EpicDependency } from '@core/typings/epics/EpicDependency';
import { RootState } from '@core/typings/reducers/RootState';
import { loggingMiddleware } from 'redoodle';
import { applyMiddleware, compose, createStore } from 'redux';
import { createEpicMiddleware } from 'redux-observable';

import { ClientApiExecutor } from '../api/ClientApiExecutor';
import { combinedEpics } from '../epics';
import { combinedReducers, initializeState } from '../reducers';

const apiExecutor = new ClientApiExecutor();

export const initStore = (initialState = initializeState()) => {
  const epicMiddleware = createEpicMiddleware<any, any, RootState, EpicDependency>({ dependencies: { apiExecutor } });
  const middlewares = [epicMiddleware, loggingMiddleware()];
  const enhancers = compose(applyMiddleware(...middlewares));
  const store = createStore(combinedReducers, initialState, enhancers);

  epicMiddleware.run(combinedEpics);

  return store;
};
