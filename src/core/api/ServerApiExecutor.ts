import { AxiosRequestConfig } from 'axios';
import { Rxios } from 'rxios';
import { Observable } from 'rxjs';

export class ServerApiExecutor {
  constructor() {
    this.axios = new Rxios({
      baseURL: 'http://www.worldgymtaiwan.com/api',
      timeout: ServerApiExecutor.TIMEOUT,
    });
  }

  private static TIMEOUT: number = 10000;
  private axios: Rxios;

  public call<T>(payload: AxiosRequestConfig): Observable<T> {
    switch (payload.method) {
      case 'GET':
      default:
        return this.axios.get<T>(payload.url, payload.params);
      case 'POST':
        return this.axios.post<T>(payload.url, payload.data);
    }
  }
}

export const serverApiExecutor = new ServerApiExecutor();
