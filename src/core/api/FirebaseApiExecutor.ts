import firebase from 'firebase-admin';

const serviceAccount = require('../../../config/firebase.json');

export class FirebaseApiExecutor {
  public database: firebase.database.Database;
  public storage: firebase.storage.Storage;

  constructor() {
    this.app = firebase.initializeApp({
      credential: firebase.credential.cert(serviceAccount),
      databaseURL: 'https://wgwtg-4dc67a.firebaseio.com',
      storageBucket: 'wgwtg-4dc67a.appspot.com',
    });
    this.database = firebase.database(this.app);
    this.storage = firebase.storage(this.app);
  }

  private app: firebase.app.App;
}

export const firebaseApiExecutor = new FirebaseApiExecutor();
