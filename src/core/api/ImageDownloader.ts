import { AxiosResponse } from 'axios';
import { Rxios, rxiosConfig } from 'rxios';
import { Observable } from 'rxjs';
import { catchError, filter, map } from 'rxjs/operators';

import { ErrorHandlerWithObservable } from '../utils/ErrorHandler';

export class ImageDownloader {
  constructor(options: rxiosConfig) {
    this.axios = new Rxios({
      ...options,
      timeout: ImageDownloader.TIMEOUT,
      responseType: 'arraybuffer',
    });
  }

  private static readonly TIMEOUT = 3000;
  private axios: Rxios;

  public download(url: string): Observable<string> {
    return this.axios
      .get<AxiosResponse<ArrayBuffer>>(url)
      .pipe(
        filter<AxiosResponse<ArrayBuffer>>(
          (response) => response.status === 200
        ),
        map<AxiosResponse<ArrayBuffer>, string>((response) =>
          Buffer.from(response.data).toString('base64')
        ),
        catchError(ErrorHandlerWithObservable())
      );
  }
}
