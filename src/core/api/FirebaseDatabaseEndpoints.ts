export const LocationDatabaseEndpoint = '/Location';
export const CourseDatabaseEndpoint = '/Course';
export const SubjectDatabaseEndpoint = '/Subject';
export const UserDatabaseEndpoint = '/User';
export const PostDatabaseEndpoint = '/Post';
export const NewsDatabaseEndpoint = '/News';
