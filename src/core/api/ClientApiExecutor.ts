import { AxiosRequestConfig } from 'axios';
import { AES } from 'crypto-js';
import { Rxios } from 'rxios';
import { Observable } from 'rxjs';

const CryptoKeys: string = require('../../../config/crypto.json');

export class ClientApiExecutor {
  constructor() {
    this.axios = new Rxios({
      baseURL: '/api',
      timeout: ClientApiExecutor.TIMEOUT,
      transformRequest: (data) => {
        return AES.encrypt(data, CryptoKeys);
      },
      transformResponse: (data) => {
        return AES.decrypt(data, CryptoKeys);
      },
    });
  }

  private static TIMEOUT: number = 30000;
  private axios: Rxios;

  public call<T>(payload: AxiosRequestConfig): Observable<T> {
    switch (payload.method) {
      case 'GET':
      default:
        return this.axios.get<T>(payload.url, payload.params);
      case 'POST':
        return this.axios.post<T>(payload.url, payload.data);
    }
  }
}

export const clientApiExecutor = new ClientApiExecutor();
