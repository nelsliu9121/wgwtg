import { CoursesType } from '@core/constants/Courses';
import { LocationsType } from '@core/constants/Locations';
import { StorageType } from '@core/constants/Storage';

export { LocationsType, CoursesType, StorageType };
