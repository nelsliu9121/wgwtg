import { configure } from 'enzyme';
import * as Adapter from 'enzyme-adapter-react-16';
import 'jest';

declare const global: any;

configure({ adapter: new Adapter() });
global.requestAnimationFrame = (callback) => {
  setTimeout(callback, 0);
};
