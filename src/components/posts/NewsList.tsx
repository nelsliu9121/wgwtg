import { NewsCard } from '@components/posts/NewsCard';
import { News } from '@core/typings/News';
import { isThisMonth } from 'date-fns';
import { List } from 'immutable';
import * as React from 'react';
import { compose, mapProps } from 'recompose';
import styled from 'styled-jss';

interface NewsListOuterProps {
  news: List<News>;
}
interface NewsListInjectedProps {}
interface NewsListState {}
interface NewsListStateHandlers {}
interface NewsListHandlers {}
type NewsListInnerProps = NewsListOuterProps &
  NewsListState &
  NewsListStateHandlers &
  NewsListInjectedProps &
  NewsListHandlers;

const NewsListComponent: React.SFC<NewsListInnerProps> = ({ news }) => (
  <NewsListWrap>{news.map((n, ni) => <NewsCard key={ni} news={n} />).toArray()}</NewsListWrap>
);

export const NewsList = compose<NewsListInnerProps, NewsListOuterProps>(
  mapProps<NewsListOuterProps, NewsListOuterProps>((props) => ({
    ...props,
    news: props.news.filter((n) => isThisMonth(n.time)).toList(),
  }))
)(NewsListComponent);

const NewsListWrap = styled('div')({});
