import { PostCard } from '@components/posts/PostCard';
import { Post } from '@core/typings/Post';
import { List } from 'immutable';
import * as React from 'react';
import { compose, defaultProps, mapProps } from 'recompose';
import styled from 'styled-jss';

interface PostsListProps {
  posts: List<Post>;
}

const PostsListComponent: React.SFC<PostsListProps> = ({ posts }) => (
  <PostsListWrap>
    {posts.map((post, index) => <PostCard key={index} post={post} setPostPinned={null} />).toArray()}
  </PostsListWrap>
);

export const PostsList = compose<PostsListProps, PostsListProps>(
  defaultProps<PostsListProps>({
    posts: List<Post>(),
  }),
  mapProps<PostsListProps, PostsListProps>((props) => props)
)(PostsListComponent);

const PostsListWrap = styled('div')({});
