import { Icon } from '@components/common/Icon';
import { Post } from '@core/typings/Post';
import { stripStyle } from '@core/utils/html/stripStyle';
import { Card, CardActions, CardContent, CardHeader, Collapse, IconButton } from '@material-ui/core';
import classnames from 'classnames';
import * as React from 'react';
import { FaChevronDown, FaThumbTack } from 'react-icons/lib/fa';
import styled from 'styled-jss';

interface PostCardProps {
  post: Post;
  setPostPinned: (post: Post) => void;
}

interface PostCardState {
  expanded: boolean;
}

export class PostCard extends React.PureComponent<PostCardProps, PostCardState> {
  public state = {
    expanded: false,
  };

  public render() {
    const { post } = this.props;
    return (
      <Card>
        <CardHeader title={post.name} subheader={post.category.name} />
        <Collapse in={this.state.expanded} timeout="auto" unmountOnExit={true}>
          <CardContent>
            <div dangerouslySetInnerHTML={{ __html: stripStyle(post.content) }} />
          </CardContent>
        </Collapse>
        <CardActions>{this.renderActions()}</CardActions>
      </Card>
    );
  }

  private renderActions(): JSX.Element {
    return (
      <>
        <IconButton color="inherit" onClick={this.setPostPinned}>
          <Icon icon={FaThumbTack} />
        </IconButton>
        <ExpandIconButton
          className={classnames({ expanded: this.state.expanded })}
          onClick={this.expandContent}
          aria-expanded={this.state.expanded}
          aria-label="Show more"
        >
          <Icon icon={FaChevronDown} />
        </ExpandIconButton>
      </>
    );
  }

  private setPostPinned() {
    this.props.setPostPinned(this.props.post);
  }

  private expandContent() {
    this.setState({
      expanded: !this.state.expanded,
    });
  }
}

const ExpandIconButton = styled(IconButton)({
  marginLeft: 'auto',
  transition: 'transform 300ms ease-in-out',
  '&.expanded': {
    transform: 'rotate(180deg)',
  },
});
