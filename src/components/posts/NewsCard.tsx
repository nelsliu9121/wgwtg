import { News } from '@core/typings/News';
import { stripStyle } from '@core/utils/html/stripStyle';
import { Card, CardContent, CardHeader, Collapse } from '@material-ui/core';
import * as classnames from 'classnames';
import { format } from 'date-fns';
import * as React from 'react';
import { compose, withState } from 'recompose';
import styled from 'styled-jss';

interface NewsCardOuterProps {
  news: News;
}

interface NewsCardState {
  expanded: boolean;
}
interface NewsCardStateHandlers {
  setExpanded: (setTo: boolean) => void;
}

type NewsCardInnerProps = NewsCardOuterProps & NewsCardState & NewsCardStateHandlers;

const NewsCardComponent: React.SFC<NewsCardInnerProps> = ({ news, expanded, setExpanded }) => (
  <StyledCard>
    <CardHeader title={news.title} subheader={format(news.time, 'YYYY-MM-DD HH:mm')} component="h3" />
    <Collapse in={expanded} collapsedHeight="60px">
      <CardContentWrap className={classnames({ expanded })} onClick={() => setExpanded(!expanded)}>
        <div dangerouslySetInnerHTML={{ __html: stripStyle(news.content) }} />
      </CardContentWrap>
    </Collapse>
  </StyledCard>
);

export const NewsCard = compose<NewsCardInnerProps, NewsCardOuterProps>(
  withState<NewsCardOuterProps, NewsCardState['expanded'], 'expanded', 'setExpanded'>('expanded', 'setExpanded', false)
)(NewsCardComponent);

const StyledCard = styled(Card)({
  margin: 10,
});

const CardContentWrap = styled(CardContent)({
  position: 'relative',
  '&:after': {
    content: '""',
    transition: 'top 600ms ease-in-out',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    top: 0,
    backgroundImage: 'linear-gradient(to bottom, rgba(0,0,0,0) 0%, rgba(0,0,0,0) 60%, rgba(255,255,255,1) 100%)',
  },
  '&.expanded:after': {
    top: '100%',
  },
});
