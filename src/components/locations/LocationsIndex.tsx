import { Icon } from '@components/common/Icon';
import { Location } from '@core/typings/Location';
import { goToLocation } from '@core/utils/routes/goToLocation';
import { GridList, GridListTile, GridListTileBar, IconButton } from '@material-ui/core';
import { List } from 'immutable';
import * as React from 'react';
import { FaArrowRight } from 'react-icons/lib/fa';

interface LocationsIndexOwnProps {
  locations: List<Location>;
}

export class LocationsIndex extends React.Component<LocationsIndexOwnProps, any> {
  public render() {
    return (
      <GridList cellHeight={180}>
        {List(this.props.locations)
          .filter((location) => Boolean(location))
          .map(this.renderTile)
          .toArray()}
      </GridList>
    );
  }

  private renderTile(location: Location, index: number): JSX.Element {
    return (
      <GridListTile key={index}>
        <img src={location.photo} alt={location.name} />
        <GridListTileBar
          title={location.name}
          subtitle={<span>by: {location.alias}</span>}
          actionIcon={
            <IconButton onClick={() => goToLocation(location)}>
              <Icon icon={FaArrowRight} />
            </IconButton>
          }
        />
      </GridListTile>
    );
  }
}
