import { Icon } from '@components/common/Icon';
import { Location } from '@core/typings/Location';
import { goToLocation } from '@core/utils/routes/goToLocation';
import { Card, CardActions, CardContent, CardMedia, IconButton, Typography } from '@material-ui/core';
import * as React from 'react';
import { FaListUl, FaStar, FaStarO } from 'react-icons/lib/fa';
import { compose } from 'recompose';
import styled from 'styled-jss';

interface LocationCardProps {
  location: Location;
  inStarList: boolean;
}

const LocationCardEnhance = compose<LocationCardProps, LocationCardProps>();

const LocationCardComponent: React.SFC<LocationCardProps> = ({
  location,
  inStarList,
}) => (
  <StyledCard>
    <StyledCardMedia
      image={location.photo}
      title={`${location.name} picture`}
    />
    <CardContent>
      <Typography variant="headline">{location.name}</Typography>
      <Typography variant="subheading">{location.alias}</Typography>
    </CardContent>
    <CardActions>
      <IconButton onClick={() => goToLocation(location)}>
        <Icon icon={FaListUl} />
      </IconButton>
      <IconButton>
        <Icon icon={inStarList ? FaStar : FaStarO} />
      </IconButton>
    </CardActions>
  </StyledCard>
);

export const LocationCard = LocationCardEnhance(LocationCardComponent);

const StyledCard = styled(Card)({
  maxWidth: 250,
});

const StyledCardMedia = styled(CardMedia)({
  minHeight: 100,
});
