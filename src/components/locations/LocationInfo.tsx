import { Icon } from '@components/common/Icon';
import { Location } from '@core/typings/Location';
import { goToCourseByClassroom } from '@core/utils/routes/goToCourseByClassroom';
import { Collapse, List as MaterialList, ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import * as React from 'react';
import { FaChevronDown, FaChevronRight, FaMapMarker, FaPhone, FaRoad } from 'react-icons/lib/fa';
import { branch, compose, renderNothing, withState } from 'recompose';
import styled from 'styled-jss';

interface LocationInfoOuterProps {
  location: Location;
}

interface LocationInfoState {
  showClassrooms: boolean;
}
interface LocationInfoStateHandlers {
  setShowClassrooms: (setTo: boolean) => void;
}

interface LocationInfoInjectedProps {}

type LocationInfoInnerProps = LocationInfoOuterProps &
  LocationInfoState &
  LocationInfoStateHandlers &
  LocationInfoInjectedProps;

const LocationInfoEnhance = compose<LocationInfoInnerProps, LocationInfoOuterProps>(
  withState<LocationInfoOuterProps, LocationInfoState['showClassrooms'], 'showClassrooms', 'setShowClassrooms'>(
    'showClassrooms',
    'setShowClassrooms',
    false
  ),
  branch<LocationInfoInnerProps>(({ location }) => !location, renderNothing)
);

const LocationInfoComponent: React.SFC<LocationInfoInnerProps> = ({ location, showClassrooms, setShowClassrooms }) => (
  <MaterialList>
    <ListItem divider={true}>
      <ListItemIcon>
        <Icon icon={FaMapMarker} />
      </ListItemIcon>
      <HeadingItemText primary="Name" />
      <ContentItemText primary={location.name} secondary={location.alias} />
    </ListItem>
    <ListItem divider={true}>
      <ListItemIcon>
        <Icon icon={FaPhone} />
      </ListItemIcon>
      <HeadingItemText primary="Phone" />
      <ContentItemText primary={location.phone} />
    </ListItem>
    <ListItem divider={true}>
      <ListItemIcon>
        <Icon icon={FaMapMarker} />
      </ListItemIcon>
      <HeadingItemText primary="Address" />
      <ContentItemText primary={location.address} />
    </ListItem>
    <ListItem divider={true} button={true} onClick={() => setShowClassrooms(!showClassrooms)}>
      <ListItemIcon>
        <Icon icon={FaRoad} />
      </ListItemIcon>
      <HeadingItemText primary="Classrooms" />
      <ContentItemText primary={<Icon icon={showClassrooms ? FaChevronDown : FaChevronRight} />} />
    </ListItem>
    <Collapse in={showClassrooms} timeout="auto" unmountOnExit={true}>
      <MaterialList component="div" disablePadding={true}>
        {location.rooms
          .map((room, ri) => (
            <ListItem button={true} key={ri} onClick={() => goToCourseByClassroom(location, room)}>
              <ListItemIcon>
                <Icon icon={FaMapMarker} />
              </ListItemIcon>
              <ListItemText inset={true} primary={room.name} />
            </ListItem>
          ))
          .toArray()}
      </MaterialList>
    </Collapse>
  </MaterialList>
);

export const LocationInfo = LocationInfoEnhance(LocationInfoComponent);

const HeadingItemText = styled(ListItemText)({});

const ContentItemText = styled(ListItemText)({
  textAlign: 'right',
});
