import { Course } from '@core/typings/Course';
import { ListItem, ListItemText } from '@material-ui/core';
import * as React from 'react';
import { compose, withHandlers, withState } from 'recompose';
import styled from 'styled-jss';

interface CourseCardOuterProps {
  course: Course;
  inStarList: boolean;
  addToStarList: (course: Course) => void;
  removeFromStarList: (course: Course) => void;
}

interface CourseCardState {}

interface CourseCardStateHandlers {}

interface CourseCardHandlers {
  toggleStar: () => void;
}

type CourseCardInnerProps = CourseCardOuterProps & CourseCardState & CourseCardStateHandlers & CourseCardHandlers;

const CourseCardEnhance = compose<CourseCardInnerProps, CourseCardOuterProps>(
  withState('expanded', 'setExpanded', false),
  withHandlers<CourseCardOuterProps & CourseCardState & CourseCardStateHandlers, CourseCardHandlers>({
    toggleStar: ({ addToStarList, removeFromStarList, inStarList, course }) => () =>
      inStarList ? removeFromStarList(course) : addToStarList(course),
  })
);

const CourseCardComponent: React.SFC<CourseCardInnerProps> = ({ course, addToStarList }) => (
  <BorderedListItem color={course.category.color}>
    <Heading primary={course.name} secondary={course.teacher} />
    <SecondaryHeading primary={course.startTime} secondary={course.endTime} />
    {/* <ListItemSecondaryAction>
      <IconButton onClick={() => addToStarList(course)}>
        <Icon icon={FaStar} />
      </IconButton>
    </ListItemSecondaryAction> */}
  </BorderedListItem>
);

export const CourseCard = CourseCardEnhance(CourseCardComponent);

const BorderedListItem = styled(ListItem)({
  borderLeft: (props) => `5px solid ${props.color}`,
});

const Heading = styled(ListItemText)({
  flex: '2 2 60%',
});

const SecondaryHeading = styled(ListItemText)({
  flex: '1 1 auto',
});
