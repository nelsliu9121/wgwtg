import { CourseCard } from '@components/courses/CourseCard';
import { WeekdayName } from '@core/typings/common/WeekdayName';
import { Course } from '@core/typings/Course';
import { Schedule } from '@core/typings/Schedule';
import { AppBar, List as MaterialList, Tab, Tabs } from '@material-ui/core';
import autobind from 'autobind-decorator';
import { List } from 'immutable';
import * as React from 'react';
import SwipeableViews from 'react-swipeable-views';

interface CoursesListProps {
  schedule: Schedule;
}

interface CoursesListState {
  currentTab: number;
}

export class CoursesList extends React.Component<CoursesListProps, CoursesListState> {
  public state = {
    currentTab: 0,
  };

  public render() {
    const { schedule } = this.props;
    return (
      <div className="courses-list">
        {this.renderTabs()}
        {schedule ? this.renderLists(schedule) : null}
      </div>
    );
  }

  private renderTabs(): JSX.Element {
    return (
      <AppBar position="static" color="default">
        <Tabs
          value={this.state.currentTab}
          onChange={this.onChangeTab}
          scrollable={false}
          scrollButtons="off"
          fullWidth={true}
        >
          {WeekdayName.All.map((weekday, i) => <Tab key={i} label={weekday.charAt(0)} />).toArray()}
        </Tabs>
      </AppBar>
    );
  }

  private renderLists(schedule: Schedule): JSX.Element {
    return (
      <SwipeableViews axis="x" index={this.state.currentTab} onChangeIndex={this.onChangeTabBySwipe}>
        {WeekdayName.All.map((_weekday, index) => {
          const weekdayName = WeekdayName.get(index + 1);
          return this.renderList(schedule.schedule.get(weekdayName), index);
        }).toArray()}
      </SwipeableViews>
    );
  }

  private renderList(course: List<Course>, index: number): JSX.Element {
    return <MaterialList key={index}>{course.map(this.renderCourseItem).toArray()}</MaterialList>;
  }

  private renderCourseItem(course: Course, index: number): JSX.Element {
    return <CourseCard key={index} course={course} inStarList={false} addToStarList={null} removeFromStarList={null} />;
  }

  @autobind
  private onChangeTab(_evt: React.ChangeEvent<any>, value: number) {
    this.setState({
      currentTab: value,
    });
  }

  @autobind
  private onChangeTabBySwipe(value: number) {
    this.setState({
      currentTab: value,
    });
  }
}
