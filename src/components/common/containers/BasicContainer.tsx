import { Icon } from '@components/common/Icon';
import { goToPage } from '@core/utils/routes/goToPage';
import {
  AppBar,
  Drawer,
  IconButton,
  List as MaterialList,
  ListItem,
  ListItemIcon,
  ListItemText,
  Toolbar,
  Typography,
} from '@material-ui/core';
import autobind from 'autobind-decorator';
import { List } from 'immutable';
import * as React from 'react';
import { FaBars, FaBullhorn, FaCalendarO, FaMapMarker, FaNewspaperO } from 'react-icons/lib/fa';
import styled from 'styled-jss';

interface BasicContainerProps {
  title?: string;
  rightSide?: JSX.Element;
}

interface BasicContainerState {
  drawerOpen: boolean;
}

interface DrawerListItem {
  icon: JSX.Element;
  label: string;
  onClick: () => void;
}

export class BasicContainer extends React.PureComponent<BasicContainerProps, BasicContainerState> {
  public state = {
    drawerOpen: false,
  };

  private static drawerItems = List<DrawerListItem>([
    {
      icon: <Icon icon={FaMapMarker} />,
      label: 'Locations',
      onClick() {
        goToPage('/locations');
      },
    },
    {
      icon: <Icon icon={FaCalendarO} />,
      label: 'Courses',
      onClick() {
        goToPage('/courses');
      },
    },
    {
      icon: <Icon icon={FaBullhorn} />,
      label: 'Posts',
      onClick() {
        goToPage('/posts');
      },
    },
    {
      icon: <Icon icon={FaNewspaperO} />,
      label: 'News',
      onClick() {
        goToPage('/posts/news');
      },
    },
  ]);

  public render() {
    return (
      <>
        <AppBar position="sticky">
          <Toolbar>
            <IconButton component="button" onClick={this.toggleDrawer} color="inherit" aria-label="Menu">
              <Icon icon={FaBars} />
            </IconButton>
            <Typography variant="title" color="inherit">
              {this.props.title || 'WGWTG'}
            </Typography>
            {this.props.rightSide}
          </Toolbar>
        </AppBar>
        <MainContent className="main">
          {this.props.children}
          {this.renderDrawer()}
        </MainContent>
      </>
    );
  }

  private renderDrawer(): JSX.Element {
    return (
      <Drawer open={this.state.drawerOpen} onClose={this.toggleDrawer}>
        <MaterialList component="nav">{BasicContainer.drawerItems.map(this.renderDrawerItem).toArray()}</MaterialList>
      </Drawer>
    );
  }

  @autobind
  private renderDrawerItem(item: DrawerListItem, index: number): JSX.Element {
    const onClickListItem = () => {
      this.toggleDrawer();
      item.onClick();
    };
    return (
      <ListItem key={index} onClick={onClickListItem}>
        <ListItemIcon>{item.icon}</ListItemIcon>
        <ListItemText primary={item.label} />
      </ListItem>
    );
  }

  @autobind
  private toggleDrawer() {
    this.setState({
      drawerOpen: !this.state.drawerOpen,
    });
  }
}

const MainContent = styled('section')({});
