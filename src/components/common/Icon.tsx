import * as React from 'react';

interface IconProps {
  icon: any;
}

export class Icon extends React.PureComponent<IconProps, any> {
  public render() {
    return this.props.icon();
  }
}
