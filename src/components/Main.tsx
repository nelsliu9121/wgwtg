import { FlexContainer } from '@components/common/containers/FlexContainer';
import { Typography } from '@material-ui/core';
import * as React from 'react';

export class Main extends React.Component<any, any> {
  public render() {
    return (
      <FlexContainer>
        <section id="favorite-locations">
          <Typography variant="title" gutterBottom={true}>
            最愛的分店
          </Typography>
          {this.renderLocations()}
        </section>
        <section id="favorite-courses">
          <Typography variant="title" gutterBottom={true}>
            最愛的課程
          </Typography>
        </section>
      </FlexContainer>
    );
  }

  private renderLocations() {
    return <div />;
  }
}
