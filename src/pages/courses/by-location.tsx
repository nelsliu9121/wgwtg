import { getMonth } from 'date-fns';
import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redoodle';

import { BasicContainer } from '../../components/common/containers/BasicContainer';
import { CoursesList } from '../../components/courses/CoursesList';
import { getCourses } from '../../core/actions/Courses';
import { initializeState } from '../../core/reducers';
import { RootState } from '../../core/typings/reducers/RootState';
import { Schedule } from '../../core/typings/Schedule';

enum CourseDisplay {
  LIST,
  CALENDAR,
}

interface CoursesByLocationPageProps {
  schedule: Schedule;
}

interface CoursesByLocationPageOwnProps {
  location: string;
  room: string;
}

interface CoursesByLocationPageDispatchProps {
  getCoursesByLocation: (location: string, room: string) => void;
}

interface CoursesByLocationPageState {
  courseDisplay: CourseDisplay;
}

type CoursesByLocationPageMergedProps = CoursesByLocationPageProps &
  CoursesByLocationPageDispatchProps &
  CoursesByLocationPageOwnProps;

class CoursesByLocationPage extends React.Component<CoursesByLocationPageMergedProps, CoursesByLocationPageState> {
  public state = {
    courseDisplay: null,
  };

  public static getInitialProps({ query }: any): CoursesByLocationPageOwnProps {
    return {
      location: query.location,
      room: query.room,
    };
  }

  public componentDidMount() {
    const { location, room } = this.props;
    if (location && room) {
      this.props.getCoursesByLocation(location, room);
    }
  }

  public render() {
    return (
      <BasicContainer title="課程表">
        {CoursesByLocationPage.renderDisplay(this.state.courseDisplay, this.props.schedule)}
      </BasicContainer>
    );
  }

  public componentWillReceiveProps(nextProps: Readonly<CoursesByLocationPageMergedProps>) {
    if (nextProps.schedule !== null) {
      this.setState({
        courseDisplay: CourseDisplay.LIST,
      });
    }
  }

  private static renderDisplay(type: CourseDisplay, schedule: Schedule): JSX.Element {
    switch (type) {
      case CourseDisplay.LIST:
        return <CoursesList schedule={schedule} />;
      default:
        return <div />;
    }
  }
}

function mapStateToProps(state: RootState = initializeState()): CoursesByLocationPageProps {
  return {
    schedule: state.Courses.schedule,
  };
}

function mapDispatchToProps(dispatch: Dispatch): CoursesByLocationPageDispatchProps {
  return {
    getCoursesByLocation(location: string, room: string) {
      const month = getMonth(Date.now());
      dispatch(getCourses({ location, room, month }));
    },
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CoursesByLocationPage);
