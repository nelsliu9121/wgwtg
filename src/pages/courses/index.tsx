import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redoodle';

import { BasicContainer } from '../../components/common/containers/BasicContainer';
import { RootState } from '../../core/typings/reducers/RootState';

interface CoursesIndexPageProps {}

interface CoursesIndexPageDispatchProps {
  getUpcomingCourses: () => void;
}

class CoursesIndexPage extends React.Component<CoursesIndexPageProps & CoursesIndexPageDispatchProps, any> {
  public render() {
    return <BasicContainer title="接下來的課程">Courses</BasicContainer>;
  }
}

function mapStateToProps(state: RootState): CoursesIndexPageProps {
  return {};
}

function mapDispatchToProps(dispatch: Dispatch): CoursesIndexPageDispatchProps {
  return {
    getUpcomingCourses() {
      console.log('getUpcomingCourses');
    },
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CoursesIndexPage);
