import { List } from 'immutable';
import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redoodle';

import { BasicContainer } from '../../components/common/containers/BasicContainer';
import { PostsList } from '../../components/posts/PostsList';
import { getPosts } from '../../core/actions/Posts';
import { initializeState } from '../../core/reducers';
import { Post } from '../../core/typings/Post';
import { RootState } from '../../core/typings/reducers/RootState';

interface PostsByLocationPageOwnProps {
  location: string;
}

interface PostsByLocationPageProps {
  posts: List<Post>;
}

interface PostsByLocationPageDispatchProps {
  getPosts: (location: string) => void;
}

type PostsByLocationPageMergedProps = PostsByLocationPageOwnProps &
  PostsByLocationPageProps &
  PostsByLocationPageDispatchProps;

class PostsByLocationPage extends React.Component<PostsByLocationPageMergedProps, any> {
  public static getInitialProps({ query }: any): PostsByLocationPageOwnProps {
    return {
      location: query.location,
    };
  }

  public componentDidMount() {
    const { location } = this.props;
    if (location) {
      this.props.getPosts(location);
    }
  }

  public render() {
    const { posts } = this.props;
    return <BasicContainer title="公告">{posts ? <PostsList posts={posts} /> : null}</BasicContainer>;
  }
}

function mapStateToProps(state: RootState = initializeState()): PostsByLocationPageProps {
  return {
    posts: state.Posts.posts,
  };
}

function mapDispatchToProps(dispatch: Dispatch): PostsByLocationPageDispatchProps {
  return {
    getPosts(location: string) {
      dispatch(getPosts({ location }));
    },
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PostsByLocationPage);
