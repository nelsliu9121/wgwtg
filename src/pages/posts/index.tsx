import { BasicContainer } from '@components/common/containers/BasicContainer';
import { getPosts } from '@core/actions/Posts';
import { RootState } from '@core/typings/reducers/RootState';
import * as React from 'react';
import { Dispatch } from 'redoodle';

interface PostsIndexPageProps {}
interface PostsIndexPageDispatchProps {
  getPosts: () => void;
}

class PostsIndexPage extends React.PureComponent<PostsIndexPageProps & PostsIndexPageDispatchProps, {}> {
  public render() {
    return (
      <BasicContainer title="公告">
        <div>Posts</div>
      </BasicContainer>
    );
  }
}

function mapStateToProps(state: RootState): PostsIndexPageProps {
  return {
    locations: state.Locations.locations,
  };
}

function mapDisatchToProps(dispatch: Dispatch): PostsIndexPageDispatchProps {
  return {
    getPosts() {
      // NO-OP
    },
  };
}

export default PostsIndexPage;
