import getPageContext from '@core/styles/getPageContext';
import { CssBaseline, MuiThemeProvider } from '@material-ui/core';
import Document, { Head, Main, NextScript } from 'next/document';
import * as React from 'react';
import Helmet from 'react-helmet';
import JssProvider from 'react-jss/lib/JssProvider';

export default class BaseDocument extends Document {
  public static getInitialProps(ctx: any) {
    const pageContext = getPageContext();
    const pageProps = ctx.renderPage((Component) => (props) => (
      <JssProvider registry={pageContext.sheetsRegistry} generateClassName={pageContext.generateClassName}>
        <MuiThemeProvider theme={pageContext.theme} sheetsManager={pageContext.sheetsManager}>
          <CssBaseline />
          <Component pageContext={pageContext} {...pageProps} />
        </MuiThemeProvider>
      </JssProvider>
    ));

    return {
      ...pageProps,
      pageContext,
      styles: (
        <style id="jss-server-side" dangerouslySetInnerHTML={{ __html: pageContext.sheetsRegistry.toString() }} />
      ),
      helmet: Helmet.renderStatic(),
    };
  }

  // should render on <html>
  private get helmetHtmlAttrComponents() {
    return this.props.helmet.htmlAttributes.toComponent();
  }

  // should render on <body>
  private get helmetBodyAttrComponents() {
    return this.props.helmet.bodyAttributes.toComponent();
  }

  // should render on <head>
  private get helmetHeadComponents() {
    return Object.keys(this.props.helmet)
      .filter((el) => el !== 'htmlAttributes' && el !== 'bodyAttributes')
      .map((el) => this.props.helmet[el].toComponent());
  }

  private get helmetJsx() {
    return (
      <Helmet
        htmlAttributes={{ lang: 'zh-TW' }}
        title="WGWTG"
        meta={[
          { name: 'viewport', content: 'width=device-width, initial-scale=1' },
          { property: 'og:title', content: 'WGWTG' },
        ]}
      />
    );
  }

  public render() {
    return (
      <html {...this.helmetHtmlAttrComponents}>
        <Head>
          {this.helmetJsx}
          {this.helmetHeadComponents}
          <link href="https://fonts.googleapis.com/css?family=Noto+Sans" rel="stylesheet" />
          {this.props.styles}
        </Head>
        <body {...this.helmetBodyAttrComponents}>
          <Main />
          <NextScript />
        </body>
      </html>
    );
  }
}
