import { List } from 'immutable';
import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redoodle';

import { BasicContainer } from '../../components/common/containers/BasicContainer';
import { LocationsIndex } from '../../components/locations/LocationsIndex';
import { getLocations } from '../../core/actions/Locations';
import { Location } from '../../core/typings/Location';
import { RootState } from '../../core/typings/reducers/RootState';

interface LocationsIndexPageProps {
  locations: List<Location>;
}

interface LocationsIndexPageDispatchProps {
  getLocations: () => void;
}

class LocationsIndexPage extends React.PureComponent<LocationsIndexPageProps & LocationsIndexPageDispatchProps, {}> {
  public componentDidMount() {
    this.props.getLocations();
  }

  public render() {
    const { locations } = this.props;

    return (
      <BasicContainer title="分店">
        <LocationsIndex locations={locations} />
      </BasicContainer>
    );
  }
}

function mapStateToProps(state: RootState): LocationsIndexPageProps {
  return {
    locations: state.Locations.locations,
  };
}

function mapDisatchToProps(dispatch: Dispatch): LocationsIndexPageDispatchProps {
  return {
    getLocations() {
      dispatch(getLocations());
    },
  };
}

export default connect(
  mapStateToProps,
  mapDisatchToProps
)(LocationsIndexPage);
