import { BasicContainer } from '@components/common/containers/BasicContainer';
import { LocationInfo } from '@components/locations/LocationInfo';
import { getLocations } from '@core/actions/Locations';
import { Location } from '@core/typings/Location';
import { RootState } from '@core/typings/reducers/RootState';
import { List } from 'immutable';
import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redoodle';

interface LocationViewPageProps {
  locations: List<Location>;
}
interface LocationViewPageDispatchProps {
  getLocations: () => void;
}
interface LocationViewPageOwnProps {
  locationId: string;
}
interface LocationViewPageState {}

class LocationViewPageComponent extends React.Component<
  LocationViewPageProps & LocationViewPageDispatchProps & LocationViewPageOwnProps,
  LocationViewPageState
> {
  public static getInitialProps({ query }: any): LocationViewPageOwnProps {
    return {
      locationId: query.location,
    };
  }

  public componentDidMount() {
    if (this.props.locations.isEmpty()) {
      this.props.getLocations();
    }
  }

  public render() {
    const { locations, locationId } = this.props;

    return (
      <BasicContainer>
        <LocationInfo location={locations.find((location) => location.id === locationId)} />
      </BasicContainer>
    );
  }
}

function mapStateToProps(state: RootState): LocationViewPageProps {
  return {
    locations: state.Locations.locations,
  };
}
function mapDispatchToProps(dispatch: Dispatch): LocationViewPageDispatchProps {
  return {
    getLocations() {
      dispatch(getLocations());
    },
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LocationViewPageComponent);
