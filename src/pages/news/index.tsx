import { BasicContainer } from '@components/common/containers/BasicContainer';
import { NewsList } from '@components/posts/NewsList';
import { getNews } from '@core/actions/Posts';
import { News } from '@core/typings/News';
import { RootState } from '@core/typings/reducers/RootState';
import { List } from 'immutable';
import * as React from 'react';
import { connect } from 'react-redux';
import { Dispatch } from 'redoodle';

interface NewsIndexPageProps {
  news: List<News>;
}
interface NewsIndexPageDispatchProps {
  getNews: () => void;
}
interface NewsIndexPageOwnProps {
  categoryId: number;
}

type NewsIndexPageMergedProps = NewsIndexPageProps & NewsIndexPageDispatchProps & NewsIndexPageOwnProps;
interface NewsIndexPageState {}

class NewsIndexPage extends React.Component<NewsIndexPageMergedProps, NewsIndexPageState> {
  public static getInitialProps({ query }: any): NewsIndexPageOwnProps {
    return {
      categoryId: query.category,
    };
  }

  public componentDidMount() {
    if (this.props.news.isEmpty()) {
      this.props.getNews();
    }
  }

  public render() {
    const { news } = this.props;
    return (
      <BasicContainer>
        <NewsList news={news} />
      </BasicContainer>
    );
  }
}

function mapStateToProps(state: RootState): NewsIndexPageProps {
  return {
    news: state.Posts.news,
  };
}

function mapDispatchToProps(dispatch: Dispatch): NewsIndexPageDispatchProps {
  return {
    getNews: () => dispatch(getNews()),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewsIndexPage);
