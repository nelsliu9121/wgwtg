import { AppContext } from '@core/AppContext';
import { RootState } from '@core/typings/reducers/RootState';
import withRedux from 'next-redux-wrapper';
import App, { Container } from 'next/app';
import * as React from 'react';
import { Provider } from 'react-redux';
import { Store } from 'redoodle';

interface BaseAppProps {
  Component: any;
  pageProps: object;
  store: Store<RootState>;
}

class BaseApp extends App<BaseAppProps> {
  public static async getInitialProps({ Component, ctx }: any) {
    let pageProps = {};

    if (Component.getInitialProps) {
      pageProps = await Component.getInitialProps(ctx);
    }

    return { pageProps };
  }

  public render() {
    const { Component, pageProps, store } = this.props;
    return (
      <Container>
        <Provider store={store}>
          <Component {...pageProps} />
        </Provider>
      </Container>
    );
  }
}

export default withRedux(AppContext.getStore)(BaseApp);
