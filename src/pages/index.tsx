import { BasicContainer } from '@components/common/containers/BasicContainer';
import { Main } from '@components/Main';
import * as React from 'react';

interface IndexPageDispatchProps {}

const IndexPage: React.SFC<IndexPageDispatchProps> = () => (
  <BasicContainer>
    <Main />
  </BasicContainer>
);

export default IndexPage;
