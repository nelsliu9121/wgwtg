import { isBefore, subMonths } from 'date-fns';
import { RequestHandler } from 'express';
import { List } from 'immutable';
import { from as ObservableFrom, never as ObservableNever, Observable, of as ObservableOf } from 'rxjs';
import { catchError, map, zip } from 'rxjs/operators';

import { LocationDatabaseEndpoint } from '../../core/api/FirebaseDatabaseEndpoints';
import { buildLocations } from '../../core/builders/buildLocations';
import { transformLocations } from '../../core/transformers/transformLocations';
import { LocationsSnapshot } from '../../core/typings/firebase/LocationsSnapshot';
import { Location } from '../../core/typings/Location';
import { LocationResponse } from '../../core/typings/responses/LocationResponse';
import { RouteDependencies } from '../../core/typings/server/RouteDependencies';
import { ErrorHandlerWithObservable } from '../../core/utils/ErrorHandler';

export function getLocationsRoute(dependencies: RouteDependencies): RequestHandler {
  const { apiExecutor, firebaseExecutor } = dependencies;
  const databaseRef = firebaseExecutor.database.ref(LocationDatabaseEndpoint);
  const storageRef = firebaseExecutor.storage.bucket();

  return async (_, res) => {
    const snapshot = await databaseRef.once('value');
    const data: LocationsSnapshot = snapshot.val();
    const isEmpty = !data;
    const isExpired = data && isBefore(data.lastUpdated, subMonths(Date.now(), 1));

    if (isEmpty || isExpired || true) {
      const apiObservable = apiExecutor.call<{ data: [LocationResponse] }>({
        method: 'GET',
        url: '/office',
      });

      const resultObservable = apiObservable.pipe(
        map((response) => {
          const locations = buildLocations(response.data);
          storeLocationsData(locations);
          return locations;
        }),
        catchError(ErrorHandlerWithObservable(res))
      );

      // const imagesObservable = apiObservable.pipe(
      //   map((response) => storeLocationImages(response.data.map((d) => ({ id: d.id, imagePath: d.image_path })))),
      //   catchError(ErrorHandlerWithObservable(res))
      // );
      const imagesObservable = ObservableOf(List());

      resultObservable
        .pipe(
          zip(imagesObservable),
          catchError(ErrorHandlerWithObservable(res))
        )
        .subscribe(
          (obs) => {
            console.log(obs[1]);

            return res.json(obs[0].toArray());
          },
          (err) => {
            res.writeHead(500, err.message);
            return res.end();
          }
        );
      // } else {
      //   res.json(data.locations);
    }
  };

  function storeLocationsData(locations: List<Location>): Observable<void> {
    if (!locations.isEmpty()) {
      const promise = databaseRef.set({
        locations: transformLocations(locations),
        lastUpdated: Date.now(),
      });
      return ObservableFrom(promise);
    }
    return ObservableNever();
  }

  function storeLocationImages(images: { id: string; imagePath: string }[]): Observable<any> {
    if (images) {
      const imagesObservable = ObservableOf(...images);
      imagesObservable.subscribe((image) => {
        const promise = storageRef.upload(
          `http://www.worldgymtaiwan.com/imagefly/w330-h240-c/uploads/${image.imagePath}`,
          {
            destination: `Location/${image.id}.jpg`,
            gzip: true,
            public: true,
          }
        );
        return ObservableFrom(promise).pipe(
          map((file) => {
            const url = file[0].getSignedUrl({
              action: 'read',
            });
            return url[0];
          }),
          map((url) => {
            const changePhotoUrlPromise = databaseRef
              .child(LocationDatabaseEndpoint)
              .child('/locations')
              .child(`/${image.id}`)
              .child('/photo')
              .update(url);
            return ObservableFrom(changePhotoUrlPromise);
          }),
          catchError(ErrorHandlerWithObservable())
        );
      });
      return imagesObservable;
    }
    return ObservableNever();
  }
}
