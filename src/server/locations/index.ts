import { Router } from 'express';

import { firebaseApiExecutor } from '../../core/api/FirebaseApiExecutor';
import { ImageDownloader } from '../../core/api/ImageDownloader';
import { serverApiExecutor } from '../../core/api/ServerApiExecutor';
import { RouteDependencies } from '../../core/typings/server/RouteDependencies';
import { getLocationsRoute } from './getLocationsRoute';

const locationRouter = Router();

const routeDependencies: RouteDependencies = {
  apiExecutor: serverApiExecutor,
  firebaseExecutor: firebaseApiExecutor,
  imageDownloader: new ImageDownloader({
    baseURL: 'http://www.worldgymtaiwan.com/imagefly/w330-h240-c/uploads/',
  }),
};

locationRouter.get('/', getLocationsRoute(routeDependencies));
// TODO:
// locationRouter.get('/by-group/:group', null);

export { locationRouter };
