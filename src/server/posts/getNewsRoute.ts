import { NewsDatabaseEndpoint } from '@core/api/FirebaseDatabaseEndpoints';
import { buildNews } from '@core/builders/buildNews';
import { buildNewsCategories } from '@core/builders/buildNewsCategories';
import { transformNews } from '@core/transformers/transformNews';
import { transformNewsCategories } from '@core/transformers/transformNewsCategories';
import { NewsSnapshot } from '@core/typings/firebase/NewsSnapshot';
import { NewsCategoriesResponse, NewsResponse } from '@core/typings/responses/NewsResponse';
import { RouteDependencies } from '@core/typings/server/RouteDependencies';
import { isBefore, subDays } from 'date-fns';
import { RequestHandler } from 'express';
import { zip as ObservableZip } from 'rxjs';
import { concatMap, map, zipAll } from 'rxjs/operators';

export function getNewsRoute({ apiExecutor, firebaseExecutor }: RouteDependencies): RequestHandler {
  return async (req, res) => {
    const databaseRef = firebaseExecutor.database.ref(NewsDatabaseEndpoint);
    const snapshot = await databaseRef.once('value');
    const data: NewsSnapshot = snapshot.val();
    const isEmpty = !data;
    const isExpired = data && isBefore(data.lastUpdated, subDays(Date.now(), 1));

    if (isEmpty || isExpired) {
      const categoriesApiObservable = apiExecutor
        .call<NewsCategoriesResponse>({
          method: 'GET',
          url: '/message_category',
        })
        .pipe(map((response) => response.data));
      const newsApiObservables = categoriesApiObservable.pipe(
        concatMap((categories) => {
          return categories.map((category) => {
            return apiExecutor
              .call<NewsResponse>({
                method: 'GET',
                url: '/message',
                params: {
                  category_id: category.id,
                  sort: '-released_at',
                },
              })
              .pipe(map((response) => response.data));
          });
        }),
        zipAll()
      );
      ObservableZip(categoriesApiObservable, newsApiObservables).subscribe(([categoriesRes, newsesRes]) => {
        const categories = buildNewsCategories(categoriesRes);
        const newses = buildNews(newsesRes);
        res.json({ categories: categories.toArray(), newses: newses.toArray() });
        databaseRef.set({
          categories: transformNewsCategories(categories),
          news: transformNews(newses),
          lastUpdated: Date.now(),
        });
      });
    } else {
      res.json({ categories: data.categories, news: data.news });
    }
  };
}
