import { isBefore, subDays } from 'date-fns';
import { RequestHandler } from 'express';

import { PostDatabaseEndpoint } from '../../core/api/FirebaseDatabaseEndpoints';
import { buildPosts } from '../../core/builders/buildPosts';
import { transformPosts } from '../../core/transformers/transformPosts';
import { PostsSnapshot } from '../../core/typings/firebase/PostsSnapshot';
import { PostsResponse } from '../../core/typings/responses/PostResponse';
import { RouteDependencies } from '../../core/typings/server/RouteDependencies';
import { ErrorHandler } from '../../core/utils/ErrorHandler';

export function getPostsRoute({ apiExecutor, firebaseExecutor }: RouteDependencies): RequestHandler {
  return async (req, res) => {
    const { location } = req.params;
    const databaseRef = firebaseExecutor.database.ref(PostDatabaseEndpoint).child(`/${location}`);
    const snapshot = await databaseRef.once('value');
    const data: PostsSnapshot = snapshot.val();
    const isEmpty = !data;
    const isExpired = data && isBefore(data.lastUpdated, subDays(Date.now(), 5));

    if (isEmpty || isExpired) {
      const apiObservable = apiExecutor.call<PostsResponse>({
        method: 'GET',
        url: '/post',
        params: {
          office_id: location,
          sort: '-released_at',
        },
      });
      apiObservable.subscribe((response) => {
        const result = buildPosts(response);

        if (!result.isEmpty()) {
          res.json(result.toArray());
          databaseRef.set(
            {
              posts: transformPosts(result),
              lastUpdated: Date.now(),
            },
            (err) => {
              if (err) {
                ErrorHandler(res)(err);
              }
            }
          );
        }
      }, ErrorHandler());
    } else {
      res.json(snapshot.val());
    }
  };
}
