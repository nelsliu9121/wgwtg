import { Router } from 'express';

import { firebaseApiExecutor } from '../../core/api/FirebaseApiExecutor';
import { serverApiExecutor } from '../../core/api/ServerApiExecutor';
import { getNewsRoute } from './getNewsRoute';
import { getPostsRoute } from './getPostsRoute';

const postsRouter = Router();

const routeDependencies = {
  apiExecutor: serverApiExecutor,
  firebaseExecutor: firebaseApiExecutor,
};

postsRouter.get('/news', getNewsRoute(routeDependencies));
postsRouter.get('/by-location/:location', getPostsRoute(routeDependencies));
// TODO:
// postsRouter.get('/by-group/:group', null);

export { postsRouter };
