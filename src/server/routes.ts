import { Router } from 'express';

import { coursesRouter } from './courses';
import { locationRouter } from './locations';
import { postsRouter } from './posts';
import { transformResponse } from './utils/transformResponse';

const apiRoutes = Router();

apiRoutes.use(transformResponse);
apiRoutes.use('/locations', locationRouter);
apiRoutes.use('/courses', coursesRouter);
apiRoutes.use('/posts', postsRouter);

export { apiRoutes };
