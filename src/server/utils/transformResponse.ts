import { AES } from 'crypto-js';

const CryptoKeys: string = require('../../core/config/crypto.json');

export function transformResponse(body: object): string {
  return AES.encrypt(JSON.stringify(body), CryptoKeys).toString();
}
