import { Router } from 'express';
import * as Next from 'next';

export function pagesRouterMaker(app: Next.Server): Router {
  const pagesRouter = Router();

  pagesRouter.get('/locations/:location', (req, res) => {
    return app.render(req, res, '/locations/view', {
      location: req.params.location,
    });
  });

  pagesRouter.get('/courses/by-location/:location/:room', (req, res) => {
    return app.render(req, res, '/courses/by-location', {
      location: req.params.location,
      room: req.params.room,
    });
  });

  pagesRouter.get('/posts/by-location/:location', (req, res) => {
    return app.render(req, res, '/posts/by-location', {
      location: req.params.location,
    });
  });

  return pagesRouter;
}
