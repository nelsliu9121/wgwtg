import { RequestHandler } from 'express';

import { CourseDatabaseEndpoint } from '../../core/api/FirebaseDatabaseEndpoints';
import { buildSchedules } from '../../core/builders/buildSchedule';
import { transformSchedules } from '../../core/transformers/transformCourses';
import { CourseAPIResponse } from '../../core/typings/responses/CourseResponse';
import { RouteDependencies } from '../../core/typings/server/RouteDependencies';
import { ErrorHandler } from '../../core/utils/ErrorHandler';

export function getCoursesByClassroomRoute({
  apiExecutor,
  firebaseExecutor,
}: RouteDependencies): RequestHandler {
  return async (req, res) => {
    const { location, room, month } = req.params;
    const year = new Date().getFullYear().toString();

    const databaseRef = firebaseExecutor.database
      .ref(CourseDatabaseEndpoint)
      .child(`/${month}`)
      .child(`/${location}-${room}`);
    const snapshot = await databaseRef.once('value');

    if (!snapshot.val()) {
      const apiObservable = apiExecutor.call<CourseAPIResponse>({
        method: 'GET',
        url: '/schedule_period/schedule',
        params: {
          office_id: location,
          classroom_id: room,
          month,
        },
      });
      apiObservable.subscribe((response) => {
        const result = buildSchedules(response, month, year);
        res.json(result);
        if (result) {
          databaseRef.set(transformSchedules(result), (err) => {
            if (err) {
              ErrorHandler(res)(err);
            }
          });
        }
      }, ErrorHandler());
    } else {
      res.json(snapshot.val());
    }
  };
}
