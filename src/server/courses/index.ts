import { Router } from 'express';

import { firebaseApiExecutor } from '../../core/api/FirebaseApiExecutor';
import { serverApiExecutor } from '../../core/api/ServerApiExecutor';
import { getCoursesByClassroomRoute } from './getCoursesByClassroomRoute';

const coursesRouter = Router();
const routeDependencies = {
  apiExecutor: serverApiExecutor,
  firebaseExecutor: firebaseApiExecutor,
};

coursesRouter.get(
  '/by-office/:location/:room/:month',
  getCoursesByClassroomRoute(routeDependencies)
);
// TODO:
// coursesRouter.get('/by-subject/:subject/:month', null);
// coursesRouter.get('/by-teacher/:teacher/:month', null);

export { coursesRouter };
