[![code style: prettier](https://img.shields.io/badge/code_style-prettier-ff69b4.svg?style=flat-square)](https://github.com/prettier/prettier)
## How to start participating?

1. `yarn install`
2. `yarn dev`

## Build sequence

1. `yarn install`
2. `yarn build`
