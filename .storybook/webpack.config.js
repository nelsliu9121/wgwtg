const path = require('path');
const rootPath = path.resolve(__dirname, '../');

module.exports = function(config, env) {
  config.module.rules.push({
    test: /\.(ts|tsx)$/,
    exclude: /node_modules/,
    loader: require.resolve('awesome-typescript-loader'),
    options: {
      useCache: true,
      configFileName: './.storybook/tsconfig.json',
    },
  });

  config.module.rules.push({
    test: /\.(png|jpg|svg)$/,
    loader: 'url-loader?limit=8192',
    exclude: /.svg$/,
  });

  config.resolve.alias = Object.assign({}, config.resolve.alias, {
    '@core': path.resolve(rootPath, './src/core'),
    '@components': path.resolve(rootPath, './src/components'),
    '@containers': path.resolve(rootPath, './src/containers'),
    '@pages': path.resolve(rootPath, './src/pages'),
  });

  config.resolve.extensions.push('.tsx', '.ts', '.js');

  return config;
};
