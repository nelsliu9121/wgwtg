import { setOptions } from '@storybook/addon-options';
import { configure as configureViewport } from '@storybook/addon-viewport';
import { addDecorator, configure } from '@storybook/react';

// automatically import all files ending in *.stories.js
const req = require.context('../stories', true, /.stories.tsx?$/);
function loadStories() {
  req.keys().forEach((filename) => req(filename));
}

setOptions({
  addonPanelInRight: true,
});

configure(loadStories, module);

configureViewport({
  defaultViewport: 'iphone6',
});
