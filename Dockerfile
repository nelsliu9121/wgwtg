FROM node:9.3-alpine
WORKDIR /usr/src/app
COPY ["package*.json", "gulpfile.js", "yarn.lock", "next.config.js", "./"]
COPY ["dist", "./dist"]
RUN yarn install --prod
EXPOSE 3000
CMD ["npm", "start"]
