const path = require('path');
const withTypescript = require('@zeit/next-typescript');

module.exports = withTypescript({
  webpack: (config, { isServer, dev }) => {
    // Fixes npm packages that depend on `fs` module
    config.node = {
      fs: 'empty',
    };

    const srcPath = dev ? 'src' : 'dist';
    config.resolve.alias = Object.assign({}, config.resolve.alias, {
      '@core': path.resolve(__dirname, srcPath, 'core'),
      '@containers': path.resolve(__dirname, srcPath, 'containers'),
      '@components': path.resolve(__dirname, srcPath, 'components'),
    });

    return config;
  },
});
