exports.default = {
  transform: {
    '^.+\\.tsx?$': 'ts-jest',
  },
  mapCoverage: true,
  testRegex: '(/__tests__/.*|(\\.|/)(test|spec))\\.(jsx?|tsx?)$',
  setupFiles: ['<rootDir>/src/core/setupTests.ts'],
  setupTestFrameworkScriptFile: '<rootDir>/src/core/setupTestFrameworkScript.ts',
  snapshotSerializers: ['enzyme-to-json/serializer'],
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
};
