const gulp = require('gulp');
const del = require('del');

gulp.task('clean', () => {
  return del([
    'dist',
    'core',
    'pages',
    'components',
    'server',
    'server.js*',
    '.next',
    '__tests__',
  ]);
});

gulp.task('clean:node_modules', () => {
  return del(['node_modules/@types/**/node_modules/**/*']);
});

gulp.task('default', gulp.series('clean', 'clean:node_modules'));
