#!/bin/sh

VERSION="$(node -pe "require('./package.json').version")"
docker build -t="nelsliu9121/wgwtg:$VERSION" .
docker tag nelsliu9121/wgwtg:$VERSION nelsliu9121/wgwtg:latest
